- LOGICNLG, ToTTo, and QTSumm Datasets in CSV Format:<br/>
https://drive.google.com/file/d/1PAwlg9wRPO-lg_rddVGWjIZ05airZLWD/view?usp=sharing

- Wikipedia Source Overview for LOGICNLG and ToTTo Datasets:<br/>
  https://docs.google.com/spreadsheets/d/1G619zqazyZBmOIQp54zqRC_kr0S9n1h4Z4UVngqYe9I/edit?usp=sharing<br/>

- Overlapped 1660 URLs for LOGICNLG and ToTTo Datasets:<br/>
  https://github.com/Bluebear77/Intern_ECLADATTA/blob/main/Dataset/LOGICNLG%20%26%20ToTTo%20%26%20QTSumm/Overlapped-URLs.csv

  
  <br/>
  LOGICNLG originally in csv:https://github.com/wenhuchen/LogicNLG/blob/master/all_csv.zip<br/>
  ToTTo  originally in jasonl:https://storage.googleapis.com/totto-public/totto_data.zip<br/>
  QTSumm  originally  in jason:https://huggingface.co/datasets/yale-nlp/QTSumm/tree/main
