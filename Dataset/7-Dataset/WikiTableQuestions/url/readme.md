Here contains the original WTQ raw json files `WikiTableQuestions-1.0.2/page`.

`url.py`: extracts the URL from each `20X-page` directory then generates `20X-page.csv` .

`merge.py`: merge all `20X-page.csv` into `WTQ.csv`.
