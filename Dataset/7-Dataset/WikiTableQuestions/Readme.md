Here contains all the relevant files for WTQ dataset URL extraction.

Reminder:in the published github `https://github.com/ppasupat/WikiTableQuestions/tree/master`, it does **NOT** contain the full dataset.

For the complete dataset, please refer to the following:

Source: [https://github.com/ppasupat/WikiTableQuestions/tree/master/page
](https://github.com/ppasupat/WikiTableQuestions/releases)

![image](https://github.com/Bluebear77/Intern_ECLADATTA/assets/119409649/2bd4eff3-4617-4862-870f-571fbb4a3876)
