#### Source:

- LOGICNLG: https://github.com/wenhuchen/LogicNLG/blob/master/all_csv.zip

- LoTNLG: https://github.com/yale-nlp/LLM-T2T/tree/main/data/LoTNLG

- FeTaQA: https://github.com/Yale-LILY/FeTaQA/tree/main/data

- ToTTo: ` wget https://storage.googleapis.com/totto-public/totto_data.zip ` >> `unzip totto_data.zip`
