# Detailed Statistical Report of Overlapped URLs

## Individual Dataset Information

- **FeTaQA**: 9333 unique URLs
- **LOGICNLG**: 10538 unique URLs
- **LOTNLG**: 804 unique URLs
- **ToTTo**: 81729 unique URLs

## Overlaps Between Datasets

### FeTaQA and LOGICNLG
![Venn Diagram of FeTaQA and LOGICNLG](venn_0_1.png)
Total overlapping URLs: 198

### FeTaQA and LOTNLG
![Venn Diagram of FeTaQA and LOTNLG](venn_0_2.png)
Total overlapping URLs: 15

### FeTaQA and ToTTo
![Venn Diagram of FeTaQA and ToTTo](venn_0_3.png)
Total overlapping URLs: 8725

### LOGICNLG and LOTNLG
![Venn Diagram of LOGICNLG and LOTNLG](venn_1_2.png)
Total overlapping URLs: 804

### LOGICNLG and ToTTo
![Venn Diagram of LOGICNLG and ToTTo](venn_1_3.png)
Total overlapping URLs: 1660

### LOTNLG and ToTTo
![Venn Diagram of LOTNLG and ToTTo](venn_2_3.png)
Total overlapping URLs: 140

### Overlap among FeTaQA, LOGICNLG, LOTNLG
![Venn Diagram](venn_0_1_2.png)
Total overlapping URLs: 15

### Overlap among FeTaQA, LOGICNLG, ToTTo
![Venn Diagram](venn_0_1_3.png)
Total overlapping URLs: 196

### Overlap among FeTaQA, LOTNLG, ToTTo
![Venn Diagram](venn_0_2_3.png)
Total overlapping URLs: 15

### Overlap among LOGICNLG, LOTNLG, ToTTo
![Venn Diagram](venn_1_2_3.png)
Total overlapping URLs: 140

## Summary for Four-Way Overlap
All four datasets have 15 common URLs.
