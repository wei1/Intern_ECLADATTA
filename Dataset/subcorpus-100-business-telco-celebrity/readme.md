Here is the subcorpus for the ECLADATTA project.

It has 3 corpora: Business, Telco and Celebrity. Each with 100 examples.

```
{
    "_index": "corpus_doc_telco100_en",
    "_id": "PLACEHOLDER_ID",
    "_source": {
        "identificationMetadata": {
            "id": "PLACEHOLDER_ID",
            "title": "PLACEHOLDER_TITLE",
            "url": [
                "PLACEHOLDER_URL_1",
                "PLACEHOLDER_URL_2"
            ],
            "version": "PLACEHOLDER_VERSION",
            "versionDate": "PLACEHOLDER_VERSION_DATE",
            "hash": "PLACEHOLDER_HASH",
            "wikidata": "PLACEHOLDER_WIKIDATA"
        },
        "descriptionMetadata": {
            "categories": [
                "PLACEHOLDER_CATEGORY_1",
                "PLACEHOLDER_CATEGORY_2",
                ...
            ],
            "language": "PLACEHOLDER_LANGUAGE",
            "source": "PLACEHOLDER_SOURCE"
        },
        "contentMetadata": {
            "format": "PLACEHOLDER_FORMAT",
            "content": "PLACEHOLDER_CONTENT",
            "text": "PLACEHOLDER_TEXT",
            "nbLinks": "PLACEHOLDER_NB_LINKS",
            "nbTables": "PLACEHOLDER_NB_TABLES",
            "nbTexts": "PLACEHOLDER_NB_TEXTS",
            "tableExtractionDate": "PLACEHOLDER_TABLE_EXTRACTION_DATE",
            "tables": [],
            "technology": "PLACEHOLDER_TECHNOLOGY",
            "textExtractionDate": "PLACEHOLDER_TEXT_EXTRACTION_DATE",
            "texts": [
                "PLACEHOLDER_TEXT_1",
                "PLACEHOLDER_TEXT_2",
                ...
            ]
        }
    }
}

```