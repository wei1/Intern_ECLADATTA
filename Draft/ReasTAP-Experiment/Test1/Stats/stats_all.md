# Statistics for Instance all<br/>
Total number of questions: 4000<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 1122, Percentage = 28.05%<br/>
- **quantifiers:** Count = 1122, Percentage = 28.05%<br/>
- **counting:** Count = 1450, Percentage = 36.25%<br/>
- **numerical_comparison:** Count = 100, Percentage = 2.50%<br/>
- **numerical_operation:** Count = 95, Percentage = 2.38%<br/>
- **date_difference:** Count = 57, Percentage = 1.43%<br/>
- **temporal_comparison:** Count = 54, Percentage = 1.35%<br/>
