# Statistics for Instance 62<br/>
Total number of questions: 106<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 34, Percentage = 32.08%<br/>
- **quantifiers:** Count = 34, Percentage = 32.08%<br/>
- **counting:** Count = 38, Percentage = 35.85%<br/>
