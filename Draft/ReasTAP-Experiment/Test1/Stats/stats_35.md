# Statistics for Instance 35<br/>
Total number of questions: 72<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 22, Percentage = 30.56%<br/>
- **quantifiers:** Count = 22, Percentage = 30.56%<br/>
- **counting:** Count = 28, Percentage = 38.89%<br/>
