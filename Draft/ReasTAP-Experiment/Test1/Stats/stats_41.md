# Statistics for Instance 41<br/>
Total number of questions: 59<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 14, Percentage = 23.73%<br/>
- **quantifiers:** Count = 14, Percentage = 23.73%<br/>
- **counting:** Count = 21, Percentage = 35.59%<br/>
- **temporal_comparison:** Count = 3, Percentage = 5.08%<br/>
- **date_difference:** Count = 1, Percentage = 1.69%<br/>
- **numerical_comparison:** Count = 3, Percentage = 5.08%<br/>
- **numerical_operation:** Count = 3, Percentage = 5.08%<br/>
