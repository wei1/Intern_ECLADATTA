# Statistics for Instance 74<br/>
Total number of questions: 31<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 19.35%<br/>
- **quantifiers:** Count = 6, Percentage = 19.35%<br/>
- **numerical_comparison:** Count = 6, Percentage = 19.35%<br/>
- **counting:** Count = 7, Percentage = 22.58%<br/>
- **numerical_operation:** Count = 6, Percentage = 19.35%<br/>
