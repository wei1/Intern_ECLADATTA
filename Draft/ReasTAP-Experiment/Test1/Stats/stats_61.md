# Statistics for Instance 61<br/>
Total number of questions: 28<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 21.43%<br/>
- **quantifiers:** Count = 6, Percentage = 21.43%<br/>
- **date_difference:** Count = 4, Percentage = 14.29%<br/>
- **counting:** Count = 9, Percentage = 32.14%<br/>
- **temporal_comparison:** Count = 3, Percentage = 10.71%<br/>
