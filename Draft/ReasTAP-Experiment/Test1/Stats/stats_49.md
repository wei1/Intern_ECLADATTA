# Statistics for Instance 49<br/>
Total number of questions: 26<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 8, Percentage = 30.77%<br/>
- **quantifiers:** Count = 7, Percentage = 26.92%<br/>
- **counting:** Count = 11, Percentage = 42.31%<br/>
