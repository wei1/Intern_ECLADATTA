# Statistics for Instance 5<br/>
Total number of questions: 30<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 20.00%<br/>
- **quantifiers:** Count = 6, Percentage = 20.00%<br/>
- **counting:** Count = 7, Percentage = 23.33%<br/>
- **numerical_comparison:** Count = 6, Percentage = 20.00%<br/>
- **numerical_operation:** Count = 5, Percentage = 16.67%<br/>
