# Statistics for Instance 98<br/>
Total number of questions: 75<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 15, Percentage = 20.00%<br/>
- **quantifiers:** Count = 15, Percentage = 20.00%<br/>
- **date_difference:** Count = 7, Percentage = 9.33%<br/>
- **counting:** Count = 24, Percentage = 32.00%<br/>
- **temporal_comparison:** Count = 3, Percentage = 4.00%<br/>
- **numerical_comparison:** Count = 6, Percentage = 8.00%<br/>
- **numerical_operation:** Count = 5, Percentage = 6.67%<br/>
