# Statistics for Instance 81<br/>
Total number of questions: 68<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 14, Percentage = 20.59%<br/>
- **quantifiers:** Count = 14, Percentage = 20.59%<br/>
- **date_difference:** Count = 5, Percentage = 7.35%<br/>
- **counting:** Count = 21, Percentage = 30.88%<br/>
- **temporal_comparison:** Count = 3, Percentage = 4.41%<br/>
- **numerical_comparison:** Count = 6, Percentage = 8.82%<br/>
- **numerical_operation:** Count = 5, Percentage = 7.35%<br/>
