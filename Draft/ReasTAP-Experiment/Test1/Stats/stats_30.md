# Statistics for Instance 30<br/>
Total number of questions: 15<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 40.00%<br/>
- **quantifiers:** Count = 5, Percentage = 33.33%<br/>
- **counting:** Count = 4, Percentage = 26.67%<br/>
