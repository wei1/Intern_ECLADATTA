# Statistics for Instance 94<br/>
Total number of questions: 53<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 10, Percentage = 18.87%<br/>
- **quantifiers:** Count = 10, Percentage = 18.87%<br/>
- **temporal_comparison:** Count = 5, Percentage = 9.43%<br/>
- **date_difference:** Count = 3, Percentage = 5.66%<br/>
- **counting:** Count = 15, Percentage = 28.30%<br/>
- **numerical_comparison:** Count = 5, Percentage = 9.43%<br/>
- **numerical_operation:** Count = 5, Percentage = 9.43%<br/>
