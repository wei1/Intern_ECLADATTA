# Statistics for Instance 19<br/>
Total number of questions: 62<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 18, Percentage = 29.03%<br/>
- **quantifiers:** Count = 18, Percentage = 29.03%<br/>
- **counting:** Count = 26, Percentage = 41.94%<br/>
