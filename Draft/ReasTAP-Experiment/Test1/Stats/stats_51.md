# Statistics for Instance 51<br/>
Total number of questions: 67<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 18, Percentage = 26.87%<br/>
- **quantifiers:** Count = 18, Percentage = 26.87%<br/>
- **counting:** Count = 25, Percentage = 37.31%<br/>
- **numerical_comparison:** Count = 3, Percentage = 4.48%<br/>
- **numerical_operation:** Count = 3, Percentage = 4.48%<br/>
