# Statistics for Instance 24<br/>
Total number of questions: 39<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 30.77%<br/>
- **quantifiers:** Count = 12, Percentage = 30.77%<br/>
- **counting:** Count = 15, Percentage = 38.46%<br/>
