# Statistics for Instance 77<br/>
Total number of questions: 75<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 16, Percentage = 21.33%<br/>
- **quantifiers:** Count = 18, Percentage = 24.00%<br/>
- **counting:** Count = 27, Percentage = 36.00%<br/>
- **numerical_comparison:** Count = 5, Percentage = 6.67%<br/>
- **numerical_operation:** Count = 5, Percentage = 6.67%<br/>
- **temporal_comparison:** Count = 2, Percentage = 2.67%<br/>
- **date_difference:** Count = 2, Percentage = 2.67%<br/>
