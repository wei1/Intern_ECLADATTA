# Statistics for Instance 39<br/>
Total number of questions: 69<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 22, Percentage = 31.88%<br/>
- **quantifiers:** Count = 22, Percentage = 31.88%<br/>
- **counting:** Count = 25, Percentage = 36.23%<br/>
