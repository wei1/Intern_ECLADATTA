# Statistics for Instance 46<br/>
Total number of questions: 59<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 18, Percentage = 30.51%<br/>
- **quantifiers:** Count = 18, Percentage = 30.51%<br/>
- **counting:** Count = 23, Percentage = 38.98%<br/>
