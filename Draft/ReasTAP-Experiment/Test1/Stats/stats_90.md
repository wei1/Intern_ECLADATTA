# Statistics for Instance 90<br/>
Total number of questions: 98<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 33, Percentage = 33.67%<br/>
- **quantifiers:** Count = 33, Percentage = 33.67%<br/>
- **counting:** Count = 32, Percentage = 32.65%<br/>
