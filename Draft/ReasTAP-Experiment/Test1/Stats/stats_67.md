# Statistics for Instance 67<br/>
Total number of questions: 76<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 24, Percentage = 31.58%<br/>
- **quantifiers:** Count = 24, Percentage = 31.58%<br/>
- **counting:** Count = 28, Percentage = 36.84%<br/>
