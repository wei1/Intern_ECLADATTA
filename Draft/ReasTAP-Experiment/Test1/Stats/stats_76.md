# Statistics for Instance 76<br/>
Total number of questions: 102<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 32, Percentage = 31.37%<br/>
- **quantifiers:** Count = 31, Percentage = 30.39%<br/>
- **counting:** Count = 39, Percentage = 38.24%<br/>
