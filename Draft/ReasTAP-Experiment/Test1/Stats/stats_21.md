# Statistics for Instance 21<br/>
Total number of questions: 10<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 4, Percentage = 40.00%<br/>
- **quantifiers:** Count = 4, Percentage = 40.00%<br/>
- **counting:** Count = 2, Percentage = 20.00%<br/>
