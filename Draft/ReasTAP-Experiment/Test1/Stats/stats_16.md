# Statistics for Instance 16<br/>
Total number of questions: 20<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 4, Percentage = 20.00%<br/>
- **quantifiers:** Count = 4, Percentage = 20.00%<br/>
- **counting:** Count = 6, Percentage = 30.00%<br/>
- **numerical_comparison:** Count = 3, Percentage = 15.00%<br/>
- **numerical_operation:** Count = 3, Percentage = 15.00%<br/>
