# Statistics for Instance 78<br/>
Total number of questions: 85<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 26, Percentage = 30.59%<br/>
- **quantifiers:** Count = 26, Percentage = 30.59%<br/>
- **counting:** Count = 33, Percentage = 38.82%<br/>
