# Statistics for Instance 72<br/>
Total number of questions: 21<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 28.57%<br/>
- **quantifiers:** Count = 6, Percentage = 28.57%<br/>
- **counting:** Count = 9, Percentage = 42.86%<br/>
