# Statistics for Instance 65<br/>
Total number of questions: 8<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 2, Percentage = 25.00%<br/>
- **quantifiers:** Count = 2, Percentage = 25.00%<br/>
- **counting:** Count = 4, Percentage = 50.00%<br/>
