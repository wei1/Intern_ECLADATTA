# Statistics for Instance 17<br/>
Total number of questions: 45<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 16, Percentage = 35.56%<br/>
- **quantifiers:** Count = 15, Percentage = 33.33%<br/>
- **counting:** Count = 14, Percentage = 31.11%<br/>
