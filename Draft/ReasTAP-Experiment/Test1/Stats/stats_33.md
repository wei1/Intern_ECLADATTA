# Statistics for Instance 33<br/>
Total number of questions: 94<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 30, Percentage = 31.91%<br/>
- **quantifiers:** Count = 30, Percentage = 31.91%<br/>
- **counting:** Count = 34, Percentage = 36.17%<br/>
