# Statistics for Instance 15<br/>
Total number of questions: 18<br/>
Total 4 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **counting:** Count = 5, Percentage = 27.78%<br/>
- **conjunction:** Count = 6, Percentage = 33.33%<br/>
- **quantifiers:** Count = 6, Percentage = 33.33%<br/>
- **date_difference:** Count = 1, Percentage = 5.56%<br/>
