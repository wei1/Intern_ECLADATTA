# Statistics for Instance 7<br/>
Total number of questions: 37<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 32.43%<br/>
- **quantifiers:** Count = 12, Percentage = 32.43%<br/>
- **counting:** Count = 13, Percentage = 35.14%<br/>
