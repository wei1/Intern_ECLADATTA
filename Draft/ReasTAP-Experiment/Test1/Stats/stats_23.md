# Statistics for Instance 23<br/>
Total number of questions: 39<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 13, Percentage = 33.33%<br/>
- **quantifiers:** Count = 13, Percentage = 33.33%<br/>
- **counting:** Count = 13, Percentage = 33.33%<br/>
