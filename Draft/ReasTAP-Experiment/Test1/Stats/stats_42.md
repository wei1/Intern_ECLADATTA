# Statistics for Instance 42<br/>
Total number of questions: 58<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 18, Percentage = 31.03%<br/>
- **quantifiers:** Count = 18, Percentage = 31.03%<br/>
- **counting:** Count = 22, Percentage = 37.93%<br/>
