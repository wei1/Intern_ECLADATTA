# Statistics for Instance 31<br/>
Total number of questions: 35<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 10, Percentage = 28.57%<br/>
- **quantifiers:** Count = 10, Percentage = 28.57%<br/>
- **counting:** Count = 15, Percentage = 42.86%<br/>
