# Statistics for Instance 87<br/>
Total number of questions: 13<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 4, Percentage = 30.77%<br/>
- **quantifiers:** Count = 4, Percentage = 30.77%<br/>
- **counting:** Count = 5, Percentage = 38.46%<br/>
