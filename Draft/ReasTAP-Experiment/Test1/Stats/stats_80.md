# Statistics for Instance 80<br/>
Total number of questions: 59<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 20.34%<br/>
- **quantifiers:** Count = 12, Percentage = 20.34%<br/>
- **counting:** Count = 18, Percentage = 30.51%<br/>
- **temporal_comparison:** Count = 3, Percentage = 5.08%<br/>
- **date_difference:** Count = 2, Percentage = 3.39%<br/>
- **numerical_comparison:** Count = 6, Percentage = 10.17%<br/>
- **numerical_operation:** Count = 6, Percentage = 10.17%<br/>
