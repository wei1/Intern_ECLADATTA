# Statistics for Instance 45<br/>
Total number of questions: 33<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 10, Percentage = 30.30%<br/>
- **quantifiers:** Count = 10, Percentage = 30.30%<br/>
- **counting:** Count = 13, Percentage = 39.39%<br/>
