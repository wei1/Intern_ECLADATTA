# Statistics for Instance 70<br/>
Total number of questions: 97<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 22, Percentage = 22.68%<br/>
- **quantifiers:** Count = 22, Percentage = 22.68%<br/>
- **counting:** Count = 29, Percentage = 29.90%<br/>
- **numerical_comparison:** Count = 6, Percentage = 6.19%<br/>
- **numerical_operation:** Count = 6, Percentage = 6.19%<br/>
- **date_difference:** Count = 6, Percentage = 6.19%<br/>
- **temporal_comparison:** Count = 6, Percentage = 6.19%<br/>
