# Statistics for Instance 83<br/>
Total number of questions: 16<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 37.50%<br/>
- **quantifiers:** Count = 5, Percentage = 31.25%<br/>
- **counting:** Count = 5, Percentage = 31.25%<br/>
