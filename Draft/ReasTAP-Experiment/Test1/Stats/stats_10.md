# Statistics for Instance 10<br/>
Total number of questions: 88<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 26, Percentage = 29.55%<br/>
- **quantifiers:** Count = 26, Percentage = 29.55%<br/>
- **counting:** Count = 36, Percentage = 40.91%<br/>
