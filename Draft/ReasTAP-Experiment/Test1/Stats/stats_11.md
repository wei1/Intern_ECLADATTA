# Statistics for Instance 11<br/>
Total number of questions: 18<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 33.33%<br/>
- **quantifiers:** Count = 6, Percentage = 33.33%<br/>
- **counting:** Count = 6, Percentage = 33.33%<br/>
