# Statistics for Instance 96<br/>
Total number of questions: 115<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 27, Percentage = 23.48%<br/>
- **quantifiers:** Count = 28, Percentage = 24.35%<br/>
- **counting:** Count = 40, Percentage = 34.78%<br/>
- **numerical_comparison:** Count = 3, Percentage = 2.61%<br/>
- **numerical_operation:** Count = 3, Percentage = 2.61%<br/>
- **temporal_comparison:** Count = 8, Percentage = 6.96%<br/>
- **date_difference:** Count = 6, Percentage = 5.22%<br/>
