# Statistics for Instance 18<br/>
Total number of questions: 14<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 4, Percentage = 28.57%<br/>
- **quantifiers:** Count = 4, Percentage = 28.57%<br/>
- **counting:** Count = 6, Percentage = 42.86%<br/>
