# Statistics for Instance 52<br/>
Total number of questions: 11<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 4, Percentage = 36.36%<br/>
- **quantifiers:** Count = 3, Percentage = 27.27%<br/>
- **counting:** Count = 4, Percentage = 36.36%<br/>
