# Statistics for Instance 20<br/>
Total number of questions: 6<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 2, Percentage = 33.33%<br/>
- **quantifiers:** Count = 2, Percentage = 33.33%<br/>
- **counting:** Count = 2, Percentage = 33.33%<br/>
