# Statistics for Instance 69<br/>
Total number of questions: 20<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 5, Percentage = 25.00%<br/>
- **quantifiers:** Count = 6, Percentage = 30.00%<br/>
- **counting:** Count = 9, Percentage = 45.00%<br/>
