# Statistics for Instance 56<br/>
Total number of questions: 75<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 24, Percentage = 32.00%<br/>
- **quantifiers:** Count = 24, Percentage = 32.00%<br/>
- **counting:** Count = 27, Percentage = 36.00%<br/>
