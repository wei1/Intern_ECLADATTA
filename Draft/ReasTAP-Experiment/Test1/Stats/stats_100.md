# Statistics for Instance 100<br/>
Total number of questions: 72<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 13, Percentage = 18.06%<br/>
- **quantifiers:** Count = 14, Percentage = 19.44%<br/>
- **temporal_comparison:** Count = 11, Percentage = 15.28%<br/>
- **date_difference:** Count = 9, Percentage = 12.50%<br/>
- **counting:** Count = 19, Percentage = 26.39%<br/>
- **numerical_comparison:** Count = 3, Percentage = 4.17%<br/>
- **numerical_operation:** Count = 3, Percentage = 4.17%<br/>
