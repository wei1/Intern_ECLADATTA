# Statistics for Instance 3<br/>
Total number of questions: 61<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 19.67%<br/>
- **quantifiers:** Count = 12, Percentage = 19.67%<br/>
- **numerical_comparison:** Count = 6, Percentage = 9.84%<br/>
- **counting:** Count = 18, Percentage = 29.51%<br/>
- **numerical_operation:** Count = 6, Percentage = 9.84%<br/>
- **date_difference:** Count = 6, Percentage = 9.84%<br/>
- **temporal_comparison:** Count = 1, Percentage = 1.64%<br/>
