# Statistics for Instance 57<br/>
Total number of questions: 5<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 2, Percentage = 40.00%<br/>
- **quantifiers:** Count = 1, Percentage = 20.00%<br/>
- **counting:** Count = 2, Percentage = 40.00%<br/>
