# Statistics for Instance 8<br/>
Total number of questions: 99<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 24, Percentage = 24.24%<br/>
- **quantifiers:** Count = 24, Percentage = 24.24%<br/>
- **counting:** Count = 34, Percentage = 34.34%<br/>
- **numerical_comparison:** Count = 6, Percentage = 6.06%<br/>
- **numerical_operation:** Count = 5, Percentage = 5.05%<br/>
- **date_difference:** Count = 3, Percentage = 3.03%<br/>
- **temporal_comparison:** Count = 3, Percentage = 3.03%<br/>
