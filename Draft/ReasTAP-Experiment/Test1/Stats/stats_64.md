# Statistics for Instance 64<br/>
Total number of questions: 63<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 17, Percentage = 26.98%<br/>
- **quantifiers:** Count = 17, Percentage = 26.98%<br/>
- **counting:** Count = 29, Percentage = 46.03%<br/>
