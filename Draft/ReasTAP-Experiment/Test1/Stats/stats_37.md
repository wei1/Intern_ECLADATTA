# Statistics for Instance 37<br/>
Total number of questions: 85<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 24, Percentage = 28.24%<br/>
- **quantifiers:** Count = 24, Percentage = 28.24%<br/>
- **numerical_comparison:** Count = 6, Percentage = 7.06%<br/>
- **counting:** Count = 25, Percentage = 29.41%<br/>
- **numerical_operation:** Count = 6, Percentage = 7.06%<br/>
