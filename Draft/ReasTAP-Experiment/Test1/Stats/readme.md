stats_i.md: statistics corresponds to each input JSON, generated by stats.py.

stats_all.md: statistics for all 100 input JSON.

find.md: The overall ranking of all input JSON, generated by find.py.
