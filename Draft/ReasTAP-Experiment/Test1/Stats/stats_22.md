# Statistics for Instance 22<br/>
Total number of questions: 5<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **counting:** Count = 3, Percentage = 60.00%<br/>
- **conjunction:** Count = 1, Percentage = 20.00%<br/>
- **quantifiers:** Count = 1, Percentage = 20.00%<br/>
