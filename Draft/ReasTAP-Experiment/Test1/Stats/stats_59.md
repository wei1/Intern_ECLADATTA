# Statistics for Instance 59<br/>
Total number of questions: 29<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 10, Percentage = 34.48%<br/>
- **quantifiers:** Count = 10, Percentage = 34.48%<br/>
- **counting:** Count = 9, Percentage = 31.03%<br/>
