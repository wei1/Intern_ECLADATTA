# Statistics for Instance 25<br/>
Total number of questions: 145<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 44, Percentage = 30.34%<br/>
- **quantifiers:** Count = 43, Percentage = 29.66%<br/>
- **counting:** Count = 58, Percentage = 40.00%<br/>
