# Statistics for Instance 26<br/>
Total number of questions: 17<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 6, Percentage = 35.29%<br/>
- **quantifiers:** Count = 6, Percentage = 35.29%<br/>
- **counting:** Count = 5, Percentage = 29.41%<br/>
