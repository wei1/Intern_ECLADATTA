# Statistics for Instance 73<br/>
Total number of questions: 78<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 25, Percentage = 32.05%<br/>
- **quantifiers:** Count = 26, Percentage = 33.33%<br/>
- **counting:** Count = 27, Percentage = 34.62%<br/>
