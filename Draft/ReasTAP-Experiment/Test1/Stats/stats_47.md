# Statistics for Instance 47<br/>
Total number of questions: 63<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 18, Percentage = 28.57%<br/>
- **quantifiers:** Count = 18, Percentage = 28.57%<br/>
- **counting:** Count = 27, Percentage = 42.86%<br/>
