# Statistics for Instance 29<br/>
Total number of questions: 56<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 21.43%<br/>
- **quantifiers:** Count = 11, Percentage = 19.64%<br/>
- **counting:** Count = 18, Percentage = 32.14%<br/>
- **numerical_comparison:** Count = 8, Percentage = 14.29%<br/>
- **numerical_operation:** Count = 7, Percentage = 12.50%<br/>
