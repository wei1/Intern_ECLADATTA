# Statistics for Instance 28<br/>
Total number of questions: 82<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 24, Percentage = 29.27%<br/>
- **quantifiers:** Count = 25, Percentage = 30.49%<br/>
- **counting:** Count = 33, Percentage = 40.24%<br/>
