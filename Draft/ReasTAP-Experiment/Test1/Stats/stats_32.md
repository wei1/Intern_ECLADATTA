# Statistics for Instance 32<br/>
Total number of questions: 49<br/>
Total 7 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 12, Percentage = 24.49%<br/>
- **quantifiers:** Count = 12, Percentage = 24.49%<br/>
- **counting:** Count = 14, Percentage = 28.57%<br/>
- **numerical_comparison:** Count = 3, Percentage = 6.12%<br/>
- **numerical_operation:** Count = 3, Percentage = 6.12%<br/>
- **temporal_comparison:** Count = 3, Percentage = 6.12%<br/>
- **date_difference:** Count = 2, Percentage = 4.08%<br/>
