# Statistics for Instance 60<br/>
Total number of questions: 50<br/>
Total 5 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 14, Percentage = 28.00%<br/>
- **quantifiers:** Count = 14, Percentage = 28.00%<br/>
- **counting:** Count = 17, Percentage = 34.00%<br/>
- **numerical_comparison:** Count = 2, Percentage = 4.00%<br/>
- **numerical_operation:** Count = 3, Percentage = 6.00%<br/>
