# Statistics for Instance 1<br/>
Total number of questions: 22<br/>
Total 3 unique reasoning types are produced.<br/>
## Reasoning Type Statistics<br/>
- **conjunction:** Count = 8, Percentage = 36.36%<br/>
- **quantifiers:** Count = 8, Percentage = 36.36%<br/>
- **counting:** Count = 6, Percentage = 27.27%<br/>
