1.  **Levenshtein distance:**
the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other.<br/>
![image](https://github.com/Bluebear77/Intern_ECLADATTA/assets/119409649/8ffd3c6f-d15e-4e20-91f8-41291e81c237)

2.   **TF-IDF:**
term frequency–inverse document frequency,is a measure of importance of a word to a document in a collection or corpus, adjusted for the fact that some words appear more frequently in general.
<br/>The inverse document frequency is a measure of how much information the word provides, i.e., how common or rare it is across all documents.The closer it is to 0, the more common a word is.

***

3.  **Ablation study**:  In artificial intelligence (AI), particularly machine learning (ML),[1] ablation is the removal of a component of an AI system. An ablation study investigates the performance of an AI system by removing certain components to understand the contribution of the component to the overall system.[2]
